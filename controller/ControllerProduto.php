	
<?php

include_once('autoloader.php');

try {
    if ($_POST['produto_nome'] == '' || $_POST['produto_fk_tipo_produto_id'] == '') {
        throw new Exception('O nome do tipo de produto é obrigatório.');
    }
    
    $produto = new Produto();
    $produto->produto_nome= $_POST['produto_nome'];
    $produto->produto_fk_tipo_produto_id= $_POST['produto_fk_tipo_produto_id'];
    if(!$produto->gravaProduto()){
        throw new Exception('Erro ao gravar produto');
    }
    if ($produto->conn->affectedRows()>0) {
        header("Location:../view/produto/index.php");
    } else {
        throw new Exception('O produto não pode ser inserido');
    }
} catch (Exception $e) {
    echo "<script language='javascript' type='text/javascript'>window.location.href='../view/produto/index.php';alert('{$e->getMessage()}');</script>";
}


?>