	
<?php

include_once('autoloader.php');

try {
    $conexao = new Conexao();
    $usuario = new Usuario($conexao);
    $usuario->usuario_login = $_POST['login'];
    $usuario->usuario_senha = $_POST['senha'];
    if ($_POST['login'] == '' || $_POST['senha'] == '') {
        throw new Exception('Login e senha não podem ser vazios.');
    }
    if ($usuario->validaAcesso()) {
        $usuario->fetch();
        session_start();
        $_SESSION['produtor_id']=$usuario->usuario_fk_produtor_id;
        $_SESSION['sessao_iniciada']=true;
        $_SESSION['logado_nome']=$usuario->usuario_nome;
        header("Location:../view/fazenda/index.php");
    } else {
        throw new Exception('Login e/ou senha incorretos');
    }
} catch (Exception $e) {
    echo "<script language='javascript' type='text/javascript'>window.location.href='../login.php';alert('{$e->getMessage()}');</script>";
}


?>