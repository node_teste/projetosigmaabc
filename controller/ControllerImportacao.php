<?php

include_once('autoloader.php');

try {
    $arquivo_tmp = $_FILES['userfile']['tmp_name'];

    $dados = file($arquivo_tmp);
    $cont = 0;
    foreach ($dados as $linha) {
        if ($cont > 0) { //ignorar cabeçalho
            $linha = trim($linha);
            $valor = explode(';', $linha);

            $fornecedor = trim($valor[0]);
            $nota = trim($valor[1]);
            $data_compra = trim($valor[2]);
            $ano = trim($valor[3]); //nao usar, esta imbutido na data de cima
            $data_vencimento = trim($valor[4]);
            $safra = trim($valor[5]);
            $tipo_produto_nome = trim($valor[6]);
            $produto_nome = trim($valor[7]);
            $produto_quantidade = trim($valor[8]);
            $unidade = trim($valor[9]);
            $produto_valor = trim($valor[10]);

            $tipo_produto = new TipoProduto();
            $tipo_produto->tipo_produto_nome = $tipo_produto_nome;
            $tipo_produto->consultaTipoProduto();
            if ($tipo_produto->conn->numRows() > 0) {
                $tipo_produto->fetch();
                $produto_fk_tipo_produto_id = $tipo_produto->tipo_produto_id;
            } else {
                $tipo_produto->gravaTipoProduto();
                $tipo_produto->fetch();
                $produto_fk_tipo_produto_id = $tipo_produto->tipo_produto_id;
            }
            
            $produto = new Produto();
            $produto->produto_nome = $produto_nome;
            $produto->produto_quantidade = $produto_quantidade;
            $produto->produto_valor = $produto_valor;
            $produto->produto_fk_tipo_produto_id = $produto_fk_tipo_produto_id;
            $produto->gravaProduto();
        }
        $cont++;
    }

    header("Location:../view/produto/index.php");
} catch (Exception $e) {
    echo "<script language='javascript' type='text/javascript'>window.location.href='../view/produto/index.php';alert('{$e->getMessage()}');</script>";
}
