	
<?php

include_once('autoloader.php');

try {
    if ($_POST['tipo_produto_nome'] == '') {
        throw new Exception('O nome do tipo de produto é obrigatório.');
    }
    
    $tipo_produto = new TipoProduto();
    $tipo_produto->tipo_produto_nome= $_POST['tipo_produto_nome'];
    if(!$tipo_produto->gravaTipoProduto()){
        throw new Exception('Erro ao gravar tipo de produto');
    }
    if ($tipo_produto->conn->affectedRows()>0) {
        header("Location:../view/produto/index.php");
    } else {
        throw new Exception('O tipo de produto não pode ser inserido');
    }
} catch (Exception $e) {
    echo "<script language='javascript' type='text/javascript'>window.location.href='../view/produto/index.php';alert('{$e->getMessage()}');</script>";
}


?>