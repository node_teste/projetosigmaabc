<?php
session_start();
spl_autoload_register('meuAutoLoaderController');
function meuAutoLoaderController($class) {
    $full_path="../model/{$class}.class.php";  
    if(!file_exists($full_path)){
        return false;
    }
    include_once $full_path;      
}
?>