	
<?php

include_once('autoloader.php');

try {
    if ($_POST['talhao_nome'] == '' || $_POST['talhao_fk_fazenda_id'] == '' || $_POST['talhao_area'] == '') {
        throw new Exception('Nenhum campo pode estar vazio.');
    }
    $conexao = new Conexao();
    $talhao = new Talhao($conexao);
    $talhao->talhao_nome= $_POST['talhao_nome'];
    $talhao->talhao_fk_fazenda_id= $_POST['talhao_fk_fazenda_id'];
    $talhao->talhao_area= $_POST['talhao_area'];
    if(!$talhao->gravaTalhao()){
        throw new Exception('Erro ao gravar talhão');
    }
    if ($talhao->conn->affectedRows()>0) {
        header("Location:../view/fazenda/index.php");
    } else {
        throw new Exception('O talhão não pode ser inserido');
    }
} catch (Exception $e) {
    echo "<script language='javascript' type='text/javascript'>window.location.href='../view/fazenda/index.php';alert('{$e->getMessage()}');</script>";
}


?>