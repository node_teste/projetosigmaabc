	
<?php

include_once('autoloader.php');

try {
    if ($_POST['fazenda_nome'] == '') {
        throw new Exception('O nome da fazenda é obrigatório.');
    }
    $conexao = new Conexao();
    $fazenda = new Fazenda($conexao);
    $fazenda->fazenda_nome= $_POST['fazenda_nome'];
    $fazenda->fazenda_fk_produtor_id= $_SESSION['produtor_id'];
    if(!$fazenda->gravaFazenda()){
        throw new Exception('Erro ao gravar fazenda');
    }
    if ($fazenda->conn->affectedRows()>0) {
        header("Location:../view/fazenda/index.php");
    } else {
        throw new Exception('A fazenda não pode ser inserida');
    }
} catch (Exception $e) {
    echo "<script language='javascript' type='text/javascript'>window.location.href='../view/fazenda/index.php';alert('{$e->getMessage()}');</script>";
}


?>