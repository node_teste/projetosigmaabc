<!DOCTYPE HTML>
<html>

<head>
    <title>Fundação ABC</title>
    <meta name="description" content="website description" />
    <meta name="keywords" content="website keywords, website keywords" />
    <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
    <link rel="stylesheet" type="text/css" href="estilo.css" />
</head>

<body>
  <div id="main">
    <?php include("view/layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>June 1st, 2014</h5>
            <p>2014 sees the redesign of our website. Take a look around and let us know what you think.<br /><a href="#">Read more</a></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">link 1</a></li>
              <li><a href="#">link 2</a></li>
              <li><a href="#">link 3</a></li>
              <li><a href="#">link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="imgs/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <form method="POST" action="controller/ControllerLogin.php">
          <h1>Login</h1>
          <p>Abaixo utilize suas credênciais para acesso:</p>
          <div class="form_settings">
            <p><span>Login</span><input class="contact" type="text" name="login" value="" /></p>
            <p><span>Senha</span><input class="contact" type="password" name="senha" value="" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Entrar" /></p>
          </div>
          <p><br /><br />NOTA: Não forneça suas credenciais de acesso a terceiros.</p>
        </form>
      </div>
    </div>
    <?php include("view/layout/footer.php"); ?>
  </div>
</body>

</html>