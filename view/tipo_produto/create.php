<!DOCTYPE HTML>
<html>

<head>
  <?php include("../layout/head.php"); ?>
</head>

<body>
  <div id="main">
    <?php include("../layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <?php include("../layout/noticia.php"); ?>
      <div id="content">
        <!-- INICIO CONTEUDO PAGINA-->
        <form method="POST" action="../../controller/ControllerTipoProduto.php">
          <h1>Cadastrar Tipo Produto</h1>
          <div class="form_settings">
            <p><span>Nome</span><input class="contact" type="text" name="tipo_produto_nome" value="" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Salvar" /></p>
          </div>
        </form>
        <!-- FIM CONTEUDO PAGINA-->
      </div>
    </div>
    <?php include("../layout/footer.php"); ?>
  </div>
</body>

</html>