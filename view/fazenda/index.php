<!DOCTYPE HTML>
<html>

<head>
  <?php include("../layout/head.php"); ?>
</head>

<body>
  <div id="main">
    <?php include("../layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <?php include("../layout/noticia.php"); ?>
      <div class='col-md-8'>
        <div id="menubarcorpo" class='col-md-12'>
          <ul id="menucorpo">
            <li><a href='../../view/talhao/create.php'>Cadastrar Talhão</a></li>
            <li><a href='../../view/fazenda/create.php'>Cadastrar Fazenda</a></li>
          </ul>
        </div>
        <div id="content">
          <!-- INICIO CONTEUDO PAGINA-->
          <?php
          $fazenda = new fazenda();
          $fazenda->fazenda_fk_produtor_id=$_SESSION['produtor_id'];
          $fazenda->consultaFazenda();
          if ($fazenda->conn->numRows() > 0) {
            while ($fazenda->fetch()) {
              echo "<h1>Fazenda: {$fazenda->fazenda_nome}</h1>";
              $talhao = new talhao();
              $talhao->talhao_fk_fazenda_id = $fazenda->fazenda_id;
              $talhao->consultaTalhao();
              if ($talhao->conn->numRows() > 0) {
                echo "<ul>";
                while ($talhao->fetch()) {
                  echo "<li>Talhão {$talhao->talhao_nome} tem {$talhao->talhao_area}m²</li>";
                }
                echo "</ul>";
              } else {
                echo "<p>Nenhum Talhão Registrado</p>";
              }
            }
          } else {
            echo "<div class='box_info aviso'>
            Aviso: Nenhuma Fazenda Registrada
          </div>
          ";
          }
          ?>
          <!-- FIM CONTEUDO PAGINA-->
          <canvas id="myChart" width="600" height="400"></canvas>
        </div>
      </div>
    </div>
    <?php include("../layout/footer.php"); ?>
  </div>
</body>

</html>