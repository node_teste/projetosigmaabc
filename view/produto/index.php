<!DOCTYPE HTML>
<html>

<head>
  <?php include("../layout/head.php"); ?>
</head>

<body>
  <div id="main">
    <?php include("../layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <?php include("../layout/noticia.php"); ?>
      <div class='col-md-8'>
        <div id="menubarcorpo" class='col-md-12'>
          <ul id="menucorpo">
            <li><a href='../../view/tipo_produto/create.php'>Cadastrar Tipo Produto</a></li>
            <li><a href='../../view/produto/create.php'>Cadastrar Produto</a></li>
          </ul>
        </div>
        <div id="content">
          <!-- INICIO CONTEUDO PAGINA-->
          <?php
          $tipo_produto = new TipoProduto();
          $tipo_produto->consultaTipoProduto();
          if ($tipo_produto->conn->numRows() > 0) {
            while ($tipo_produto->fetch()) {
              echo "<h1>Tipo de produto: {$tipo_produto->tipo_produto_nome}</h1>";
              $produto = new Produto();
              $produto->produto_fk_tipo_produto_id = $tipo_produto->tipo_produto_id;
              $produto->consultaUnidadesProduto();
              if ($produto->conn->numRows() > 0) {
                echo "<ul>";
                while ($produto->fetch()) {
                  echo "<li>Produto {$produto->produto_nome} tem {$produto->produto_quantidade} unidades</li>";
                }
                echo "</ul>";
              } else {
                echo "<p>Nenhum Produto Registrado</p>";
              }
            }
          } else {
            echo "<div class='box_info aviso'>
            Aviso: Nenhuma Tipo Produto Registrada
          </div>
          ";
          }
          ?>
          <!-- FIM CONTEUDO PAGINA-->
        </div>
      </div>
    </div>
    <?php include("../layout/footer.php"); ?>
  </div>
</body>

</html>