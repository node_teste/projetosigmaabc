<!DOCTYPE HTML>
<html>

<head>
  <?php include("../layout/head.php"); ?>
</head>

<body>
  <div id="main">
    <?php include("../layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <?php include("../layout/noticia.php"); ?>
      <div id="content">
        <!-- INICIO CONTEUDO PAGINA-->
        <form method="POST" action="../../controller/ControllerProduto.php">
          <h1>Cadastrar Produto</h1>
          <div class="form_settings">
            <p><span>Nome</span><input class="contact" type="text" name="produto_nome" value="" /></p>
            <?php
            $tipo_produto = new TipoProduto();
            $tipo_produto->consultaTipoProduto();
            echo "<p><span>Produto</span><select class='contact' name='produto_fk_tipo_produto_id'>";
            while($tipo_produto->fetch()){
              echo "<option value='{$tipo_produto->tipo_produto_id}'>{$tipo_produto->tipo_produto_nome}</option>";
            }
            echo "</p>
            </select>";
            ?>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Salvar" /></p>
          </div>
        </form>
        <!-- FIM CONTEUDO PAGINA-->
      </div>
    </div>
    <?php include("../layout/footer.php"); ?>
  </div>
</body>

</html>