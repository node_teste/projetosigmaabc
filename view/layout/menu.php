<div id="header">
    <div id="logo">
        <div id="logo_text">
            <h1><a href="index.php">Fundação<span class="logo_colour">ABC</span></a></h1>
            <h2>Projeto SigmaABC.</h2>
        </div>
    </div>
    <div id="menubar">
        <ul id="menu">
            <?php
                if(isset($_SESSION['sessao_iniciada'])){
                    echo "
                    <li><a href='../../view/fazenda/index.php'>Fazenda</a></li>
                    <li><a href='../../view/produto/index.php'>Produtos</a></li>
                    <li><a href='../../view/importacao/create.php'>Importação</a></li>
                    <li><a href='../../controller/ControllerLogout.php'>Sair</a></li>";
                }else{
                    echo "<li><a href='index.php'>Sobre</a></li>
                    <li><a href='login.php'>Login</a></li>
                    <li><a href='#'>Entre em contato</a></li>";
                }
            ?>
        </ul>
    </div>
</div>
