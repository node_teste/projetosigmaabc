<!DOCTYPE HTML>
<html>

<head>
  <?php include("../layout/head.php"); ?>
</head>

<body>
  <div id="main">
    <?php include("../layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <?php include("../layout/noticia.php"); ?>
      <div id="content">
        <!-- INICIO CONTEUDO PAGINA-->
        <form method="POST" action="../../controller/ControllerTalhao.php">
          <h1>Cadastrar Talhão</h1>
          <div class="form_settings">
            <p><span>Nome</span><input class="contact" type="text" name="talhao_nome" value="" /></p>
            <p><span>Área</span><input class="contact" type="number" name="talhao_area" value="" /></p>
            <?php
            $conexao = new conexao();
            $fazenda = new fazenda($conexao);
            $fazenda->fazenda_fk_produtor_id=$_SESSION['produtor_id'];
            $fazenda->consultaFazenda();
            echo "<p><span>Fazenda</span><select class='contact' name='talhao_fk_fazenda_id'>";
            while($fazenda->fetch()){
              echo "<option value='{$fazenda->fazenda_id}'>{$fazenda->fazenda_nome}</option>";
            }
            echo "</p>
            </select>";
            ?>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Salvar" /></p>
          </div>
        </form>
        <!-- FIM CONTEUDO PAGINA-->
      </div>
    </div>
    <?php include("../layout/footer.php"); ?>
  </div>
</body>

</html>