<?php

class Conta extends Conexao{

    public $valor;
    public $conta_saldo;

    public function __construct($conn=null){
        if(!empty($conn)){
            $this->conn=$conn;
        }else{
            $this->conn= new conexao();
        }
    }

    public function consultaConta(){
        $sql = "SELECT * FROM conta";
        return $this->result=pg_query($this->conn,$sql);

    }
}
