<?php

class Fazenda{

    public $fazenda_nome;
    public $fazenda_id;
    public $fazenda_fk_produtor_id;
    public $conn;

    public function __construct($conn=null){
        if(!empty($conn)){
            $this->conn=$conn;
        }else{
            $this->conn= new conexao();
        }
    }
    public function fetch(){
        $res= pg_fetch_object($this->conn->result);
        if($res){
            $values = get_object_vars($res);
            foreach ($values as $k => $value) {
               $this->{$k} = $value;
           }
           return $res;
       }
       return false;

   }
    public function consultaFazenda(){
        $sql = "SELECT * FROM fazenda WHERE fazenda_fk_produtor_id=".$this->fazenda_fk_produtor_id;
        return $this->conn->result=pg_query($this->conn->conn,$sql);

    }
    public function gravaFazenda(){
        $sql = "INSERT INTO fazenda(fazenda_nome, fazenda_fk_produtor_id)
            VALUES ('{$this->fazenda_nome}', {$this->fazenda_fk_produtor_id});";
        return $this->conn->result=pg_query($this->conn->conn,$sql);

    }
    
}
