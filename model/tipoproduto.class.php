<?php

class TipoProduto{

    public $tipo_produto_nome;
    public $tipo_produto_id;
    public $conn;

    public function __construct($conn=null){
        if(!empty($conn)){
            $this->conn=$conn;
        }else{
            $this->conn= new conexao();
        }
    }
    public function fetch(){
        $res= pg_fetch_object($this->conn->result);
        if($res){
            $values = get_object_vars($res);
            foreach ($values as $k => $value) {
               $this->{$k} = $value;
           }
           return $res;
       }
       return false;

   }
    public function consultaTipoProduto(){
        $sql = "SELECT * FROM tipo_produto WHERE 1=1";
        if($this->tipo_produto_nome!=''){
            $sql.=" AND tipo_produto_nome='{$this->tipo_produto_nome}'";
        }
        return $this->conn->result=pg_query($this->conn->conn,$sql);

    }
    public function gravaTipoProduto(){
        $sql = "INSERT INTO tipo_produto(tipo_produto_nome)
            VALUES ('{$this->tipo_produto_nome}') RETURNING tipo_produto_id;";
        return $this->conn->result=pg_query($this->conn->conn,$sql);

    }
    
}
