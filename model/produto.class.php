<?php

class Produto
{

    public $produto_nome;
    public $produto_id;
    public $produto_fk_tipo_produto_id;
    public $produto_valor;
    public $produto_quantidade;
    public $conn;

    public function __construct($conn = null)
    {
        if (!empty($conn)) {
            $this->conn = $conn;
        } else {
            $this->conn = new conexao();
        }
    }
    public function fetch()
    {
        $res = pg_fetch_object($this->conn->result);
        if ($res) {
            $values = get_object_vars($res);
            foreach ($values as $k => $value) {
                $this->{$k} = $value;
            }
            return $res;
        }
        return false;
    }
    public function consultaProduto()
    {
        $sql = "SELECT * FROM produto WHERE 1=1";
        if ($this->produto_fk_tipo_produto_id != '') {
            $sql .= " AND produto_fk_tipo_produto_id={$this->produto_fk_tipo_produto_id}";
        }
        return $this->conn->result = pg_query($this->conn->conn, $sql);
    }
    public function consultaUnidadesProduto()
    {

        if ($this->produto_fk_tipo_produto_id != '') {
            $and = " AND produto_fk_tipo_produto_id={$this->produto_fk_tipo_produto_id}";
        }

        $sql = "SELECT SUM(produto_quantidade) as produto_quantidade, produto_nome FROM produto WHERE 1=1 {$and} GROUP BY produto_fk_tipo_produto_id, produto_nome";
        return $this->conn->result = pg_query($this->conn->conn, $sql);
    }
    public function gravaProduto()
    {
        if ($this->produto_valor == '') {
            $this->produto_valor = 0;
        } else {
            $this->produto_valor = str_replace(",", ".", $this->produto_valor);
        }
        if ($this->produto_quantidade == '') {
            $this->produto_quantidade = 0;
        }
        $sql = "INSERT INTO produto(produto_nome, produto_fk_tipo_produto_id,produto_valor,produto_quantidade)
            VALUES ('{$this->produto_nome}', {$this->produto_fk_tipo_produto_id},'{$this->produto_valor}',{$this->produto_quantidade});";
        return $this->conn->result = pg_query($this->conn->conn, $sql);
    }
}
