--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12
-- Dumped by pg_dump version 12.0

-- Started on 2020-03-16 02:01:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2915 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

--
-- TOC entry 214 (class 1259 OID 16508)
-- Name: compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compra (
    compra_id integer NOT NULL,
    compra_vencimento date,
    compra_fk_item_id integer,
    compra_fk_produtor_id integer
);


ALTER TABLE public.compra OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16506)
-- Name: compra_compra_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compra_compra_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compra_compra_id_seq OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 0)
-- Dependencies: 213
-- Name: compra_compra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.compra_compra_id_seq OWNED BY public.compra.compra_id;


--
-- TOC entry 206 (class 1259 OID 16449)
-- Name: conta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conta (
    conta_id integer NOT NULL,
    conta_saldo numeric,
    conta_fk_produtor_id integer
);


ALTER TABLE public.conta OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16447)
-- Name: conta_conta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.conta_conta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conta_conta_id_seq OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 0)
-- Dependencies: 205
-- Name: conta_conta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.conta_conta_id_seq OWNED BY public.conta.conta_id;


--
-- TOC entry 202 (class 1259 OID 16417)
-- Name: fazenda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fazenda (
    fazenda_id integer NOT NULL,
    fazenda_nome text,
    fazenda_fk_produtor_id integer
);


ALTER TABLE public.fazenda OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16415)
-- Name: fazenda_fazenda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fazenda_fazenda_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fazenda_fazenda_id_seq OWNER TO postgres;

--
-- TOC entry 2918 (class 0 OID 0)
-- Dependencies: 201
-- Name: fazenda_fazenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fazenda_fazenda_id_seq OWNED BY public.fazenda.fazenda_id;


--
-- TOC entry 212 (class 1259 OID 16492)
-- Name: item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item (
    item_id integer NOT NULL,
    item_quantidade numeric,
    item_valor numeric,
    item_fk_produto_id integer
);


ALTER TABLE public.item OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16490)
-- Name: item_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_item_id_seq OWNER TO postgres;

--
-- TOC entry 2919 (class 0 OID 0)
-- Dependencies: 211
-- Name: item_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.item_item_id_seq OWNED BY public.item.item_id;


--
-- TOC entry 210 (class 1259 OID 16476)
-- Name: produto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produto (
    produto_id integer NOT NULL,
    produto_nome text,
    produto_valor double precision,
    produto_quantidade numeric,
    produto_fk_tipo_produto_id integer
);


ALTER TABLE public.produto OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16474)
-- Name: produto_produto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.produto_produto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.produto_produto_id_seq OWNER TO postgres;

--
-- TOC entry 2920 (class 0 OID 0)
-- Dependencies: 209
-- Name: produto_produto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.produto_produto_id_seq OWNED BY public.produto.produto_id;


--
-- TOC entry 200 (class 1259 OID 16406)
-- Name: produtor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produtor (
    produtor_id integer NOT NULL,
    produtor_nome text
);


ALTER TABLE public.produtor OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16404)
-- Name: produtor_produtor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.produtor_produtor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.produtor_produtor_id_seq OWNER TO postgres;

--
-- TOC entry 2921 (class 0 OID 0)
-- Dependencies: 199
-- Name: produtor_produtor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.produtor_produtor_id_seq OWNED BY public.produtor.produtor_id;


--
-- TOC entry 204 (class 1259 OID 16433)
-- Name: talhao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.talhao (
    talhao_id integer NOT NULL,
    talhao_nome text,
    talhao_area integer,
    talhao_fk_fazenda_id integer
);


ALTER TABLE public.talhao OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16431)
-- Name: talhao_talhao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.talhao_talhao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.talhao_talhao_id_seq OWNER TO postgres;

--
-- TOC entry 2922 (class 0 OID 0)
-- Dependencies: 203
-- Name: talhao_talhao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.talhao_talhao_id_seq OWNED BY public.talhao.talhao_id;


--
-- TOC entry 208 (class 1259 OID 16465)
-- Name: tipo_produto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_produto (
    tipo_produto_id integer NOT NULL,
    tipo_produto_nome text
);


ALTER TABLE public.tipo_produto OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16463)
-- Name: tipo_produto_tipo_produto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_produto_tipo_produto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_produto_tipo_produto_id_seq OWNER TO postgres;

--
-- TOC entry 2923 (class 0 OID 0)
-- Dependencies: 207
-- Name: tipo_produto_tipo_produto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_produto_tipo_produto_id_seq OWNED BY public.tipo_produto.tipo_produto_id;


--
-- TOC entry 198 (class 1259 OID 16396)
-- Name: todos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.todos (
    id integer NOT NULL,
    text text,
    done boolean,
    number numeric
);


ALTER TABLE public.todos OWNER TO postgres;

--
-- TOC entry 2924 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE todos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.todos IS 'Lista do que fazer';


--
-- TOC entry 2925 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN todos.number; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.todos.number IS 'numero';


--
-- TOC entry 197 (class 1259 OID 16394)
-- Name: todos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.todos ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.todos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 216 (class 1259 OID 16525)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    usuario_id integer NOT NULL,
    usuario_login text,
    usuario_senha character varying(10),
    usuario_nome text,
    usuario_data_cadastro date,
    usuario_fk_produtor_id integer
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16523)
-- Name: usuario_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2926 (class 0 OID 0)
-- Dependencies: 215
-- Name: usuario_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_usuario_id_seq OWNED BY public.usuario.usuario_id;


--
-- TOC entry 2741 (class 2604 OID 16511)
-- Name: compra compra_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compra ALTER COLUMN compra_id SET DEFAULT nextval('public.compra_compra_id_seq'::regclass);


--
-- TOC entry 2737 (class 2604 OID 16452)
-- Name: conta conta_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conta ALTER COLUMN conta_id SET DEFAULT nextval('public.conta_conta_id_seq'::regclass);


--
-- TOC entry 2735 (class 2604 OID 16420)
-- Name: fazenda fazenda_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fazenda ALTER COLUMN fazenda_id SET DEFAULT nextval('public.fazenda_fazenda_id_seq'::regclass);


--
-- TOC entry 2740 (class 2604 OID 16495)
-- Name: item item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item ALTER COLUMN item_id SET DEFAULT nextval('public.item_item_id_seq'::regclass);


--
-- TOC entry 2739 (class 2604 OID 16479)
-- Name: produto produto_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produto ALTER COLUMN produto_id SET DEFAULT nextval('public.produto_produto_id_seq'::regclass);


--
-- TOC entry 2734 (class 2604 OID 16409)
-- Name: produtor produtor_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produtor ALTER COLUMN produtor_id SET DEFAULT nextval('public.produtor_produtor_id_seq'::regclass);


--
-- TOC entry 2736 (class 2604 OID 16436)
-- Name: talhao talhao_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.talhao ALTER COLUMN talhao_id SET DEFAULT nextval('public.talhao_talhao_id_seq'::regclass);


--
-- TOC entry 2738 (class 2604 OID 16468)
-- Name: tipo_produto tipo_produto_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_produto ALTER COLUMN tipo_produto_id SET DEFAULT nextval('public.tipo_produto_tipo_produto_id_seq'::regclass);


--
-- TOC entry 2742 (class 2604 OID 16528)
-- Name: usuario usuario_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN usuario_id SET DEFAULT nextval('public.usuario_usuario_id_seq'::regclass);


--
-- TOC entry 2907 (class 0 OID 16508)
-- Dependencies: 214
-- Data for Name: compra; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2899 (class 0 OID 16449)
-- Dependencies: 206
-- Data for Name: conta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.conta (conta_id, conta_saldo, conta_fk_produtor_id) VALUES (1, 100, 1);
INSERT INTO public.conta (conta_id, conta_saldo, conta_fk_produtor_id) VALUES (2, 1000, 1);


--
-- TOC entry 2895 (class 0 OID 16417)
-- Dependencies: 202
-- Data for Name: fazenda; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.fazenda (fazenda_id, fazenda_nome, fazenda_fk_produtor_id) VALUES (1, 'Rancho do Murilo', 1);
INSERT INTO public.fazenda (fazenda_id, fazenda_nome, fazenda_fk_produtor_id) VALUES (2, 'Riachão', 1);
INSERT INTO public.fazenda (fazenda_id, fazenda_nome, fazenda_fk_produtor_id) VALUES (3, 'Sítio', 1);
INSERT INTO public.fazenda (fazenda_id, fazenda_nome, fazenda_fk_produtor_id) VALUES (4, 'Sítio 2', 1);
INSERT INTO public.fazenda (fazenda_id, fazenda_nome, fazenda_fk_produtor_id) VALUES (5, 'Fazenda Rio Grande', 1);


--
-- TOC entry 2905 (class 0 OID 16492)
-- Dependencies: 212
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2903 (class 0 OID 16476)
-- Dependencies: 210
-- Data for Name: produto; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (15, 'Oleo Diesel', 0, 0, 17);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (16, 'Agral', 17.16, 117, 18);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (17, 'Assist', 13.970000000000001, 1397, 18);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (18, 'Aureo', 12.26, 4000, 18);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (19, 'Aureo', 12.26, 275, 18);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (20, 'Assist', 13.970000000000001, 149, 18);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (21, 'Aureo', 12.26, 89, 18);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (22, 'Oleo_Diesel', 3.2999999999999998, 81600, 19);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (23, '10-40-00', 1.3400000000000001, 222200, 20);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (24, '25-00-25', 1.4299999999999999, 142450, 20);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (25, '25-00-25', 1.5900000000000001, 30000, 20);
INSERT INTO public.produto (produto_id, produto_nome, produto_valor, produto_quantidade, produto_fk_tipo_produto_id) VALUES (26, '6-30-10', 1.3200000000000001, 233750, 20);


--
-- TOC entry 2893 (class 0 OID 16406)
-- Dependencies: 200
-- Data for Name: produtor; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.produtor (produtor_id, produtor_nome) VALUES (1, 'Murilo');


--
-- TOC entry 2897 (class 0 OID 16433)
-- Dependencies: 204
-- Data for Name: talhao; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.talhao (talhao_id, talhao_nome, talhao_area, talhao_fk_fazenda_id) VALUES (1, 'A', 50, 1);
INSERT INTO public.talhao (talhao_id, talhao_nome, talhao_area, talhao_fk_fazenda_id) VALUES (2, 'B', 155, 1);
INSERT INTO public.talhao (talhao_id, talhao_nome, talhao_area, talhao_fk_fazenda_id) VALUES (3, 'C', 108, 1);
INSERT INTO public.talhao (talhao_id, talhao_nome, talhao_area, talhao_fk_fazenda_id) VALUES (4, 'D', 500, 1);
INSERT INTO public.talhao (talhao_id, talhao_nome, talhao_area, talhao_fk_fazenda_id) VALUES (5, '1', 500, 2);
INSERT INTO public.talhao (talhao_id, talhao_nome, talhao_area, talhao_fk_fazenda_id) VALUES (6, 'norte', 350, 4);


--
-- TOC entry 2901 (class 0 OID 16465)
-- Dependencies: 208
-- Data for Name: tipo_produto; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tipo_produto (tipo_produto_id, tipo_produto_nome) VALUES (17, 'Combustível');
INSERT INTO public.tipo_produto (tipo_produto_id, tipo_produto_nome) VALUES (18, 'Adjvante');
INSERT INTO public.tipo_produto (tipo_produto_id, tipo_produto_nome) VALUES (19, 'Combustivel');
INSERT INTO public.tipo_produto (tipo_produto_id, tipo_produto_nome) VALUES (20, 'Fertilizante');


--
-- TOC entry 2891 (class 0 OID 16396)
-- Dependencies: 198
-- Data for Name: todos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.todos (id, text, done, number) OVERRIDING SYSTEM VALUE VALUES (1, 'one', false, 1);
INSERT INTO public.todos (id, text, done, number) OVERRIDING SYSTEM VALUE VALUES (2, 'two', false, 2);
INSERT INTO public.todos (id, text, done, number) OVERRIDING SYSTEM VALUE VALUES (3, 'three', false, 3);


--
-- TOC entry 2909 (class 0 OID 16525)
-- Dependencies: 216
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.usuario (usuario_id, usuario_login, usuario_senha, usuario_nome, usuario_data_cadastro, usuario_fk_produtor_id) VALUES (1, 'murilo', 'murilo28', 'Murilo Alvaro', '2020-03-14', 1);


--
-- TOC entry 2927 (class 0 OID 0)
-- Dependencies: 213
-- Name: compra_compra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compra_compra_id_seq', 1, false);


--
-- TOC entry 2928 (class 0 OID 0)
-- Dependencies: 205
-- Name: conta_conta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.conta_conta_id_seq', 2, true);


--
-- TOC entry 2929 (class 0 OID 0)
-- Dependencies: 201
-- Name: fazenda_fazenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fazenda_fazenda_id_seq', 5, true);


--
-- TOC entry 2930 (class 0 OID 0)
-- Dependencies: 211
-- Name: item_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.item_item_id_seq', 1, false);


--
-- TOC entry 2931 (class 0 OID 0)
-- Dependencies: 209
-- Name: produto_produto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.produto_produto_id_seq', 26, true);


--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 199
-- Name: produtor_produtor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.produtor_produtor_id_seq', 1, false);


--
-- TOC entry 2933 (class 0 OID 0)
-- Dependencies: 203
-- Name: talhao_talhao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.talhao_talhao_id_seq', 6, true);


--
-- TOC entry 2934 (class 0 OID 0)
-- Dependencies: 207
-- Name: tipo_produto_tipo_produto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_produto_tipo_produto_id_seq', 20, true);


--
-- TOC entry 2935 (class 0 OID 0)
-- Dependencies: 197
-- Name: todos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.todos_id_seq', 3, true);


--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 215
-- Name: usuario_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_usuario_id_seq', 1, true);


--
-- TOC entry 2760 (class 2606 OID 16513)
-- Name: compra compra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_pkey PRIMARY KEY (compra_id);


--
-- TOC entry 2752 (class 2606 OID 16457)
-- Name: conta conta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conta
    ADD CONSTRAINT conta_pkey PRIMARY KEY (conta_id);


--
-- TOC entry 2748 (class 2606 OID 16425)
-- Name: fazenda fazenda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fazenda
    ADD CONSTRAINT fazenda_pkey PRIMARY KEY (fazenda_id);


--
-- TOC entry 2758 (class 2606 OID 16500)
-- Name: item item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);


--
-- TOC entry 2756 (class 2606 OID 16484)
-- Name: produto produto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produto
    ADD CONSTRAINT produto_pkey PRIMARY KEY (produto_id);


--
-- TOC entry 2746 (class 2606 OID 16414)
-- Name: produtor produtor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produtor
    ADD CONSTRAINT produtor_pkey PRIMARY KEY (produtor_id);


--
-- TOC entry 2750 (class 2606 OID 16441)
-- Name: talhao talhao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.talhao
    ADD CONSTRAINT talhao_pkey PRIMARY KEY (talhao_id);


--
-- TOC entry 2754 (class 2606 OID 16473)
-- Name: tipo_produto tipo_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_produto
    ADD CONSTRAINT tipo_produto_pkey PRIMARY KEY (tipo_produto_id);


--
-- TOC entry 2744 (class 2606 OID 16403)
-- Name: todos todos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (id);


--
-- TOC entry 2762 (class 2606 OID 16533)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuario_id);


--
-- TOC entry 2765 (class 2606 OID 16458)
-- Name: conta conta_conta_fk_produtor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conta
    ADD CONSTRAINT conta_conta_fk_produtor_id_fkey FOREIGN KEY (conta_fk_produtor_id) REFERENCES public.produtor(produtor_id);


--
-- TOC entry 2763 (class 2606 OID 16426)
-- Name: fazenda fazenda_fazenda_fk_produtor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fazenda
    ADD CONSTRAINT fazenda_fazenda_fk_produtor_id_fkey FOREIGN KEY (fazenda_fk_produtor_id) REFERENCES public.produtor(produtor_id);


--
-- TOC entry 2767 (class 2606 OID 16501)
-- Name: item item_item_fk_produto_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_item_fk_produto_id_fkey FOREIGN KEY (item_fk_produto_id) REFERENCES public.produto(produto_id);


--
-- TOC entry 2766 (class 2606 OID 16485)
-- Name: produto produto_produto_fk_tipo_produto_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produto
    ADD CONSTRAINT produto_produto_fk_tipo_produto_id_fkey FOREIGN KEY (produto_fk_tipo_produto_id) REFERENCES public.tipo_produto(tipo_produto_id);


--
-- TOC entry 2764 (class 2606 OID 16442)
-- Name: talhao talhao_talhao_fk_fazenda_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.talhao
    ADD CONSTRAINT talhao_talhao_fk_fazenda_id_fkey FOREIGN KEY (talhao_fk_fazenda_id) REFERENCES public.fazenda(fazenda_id);


--
-- TOC entry 2768 (class 2606 OID 16541)
-- Name: usuario usuario_usuario_fk_produtor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_usuario_fk_produtor_id_fkey FOREIGN KEY (usuario_fk_produtor_id) REFERENCES public.produtor(produtor_id) NOT VALID;


-- Completed on 2020-03-16 02:01:17

--
-- PostgreSQL database dump complete
--

