<!DOCTYPE HTML>
<html>

<head>
    <title>Fundação ABC</title>
    <meta name="description" content="website description" />
    <meta name="keywords" content="website keywords, website keywords" />
    <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
    <link rel="stylesheet" type="text/css" href="estilo.css" />
</head>

<body>
  <div id="main">
    <?php include("view/layout/menu.php"); ?>
    <div id="content_header"></div>
    <div id="site_content">
      <!-- <div id="banner"></div> -->
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>June 1st, 2014</h5>
            <p>2014 sees the redesign of our website. Take a look around and let us know what you think.<br /><a href="#">Read more</a></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">link 1</a></li>
              <li><a href="#">link 2</a></li>
              <li><a href="#">link 3</a></li>
              <li><a href="#">link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="imgs/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>A Fundação ABC</h1>
        <p>A Fundação ABC é uma instituição de caráter particular, sem fins lucrativos, que realiza pesquisa aplicada para desenvolver e adaptar novas tecnologias, com o objetivo de promover soluções tecnológicas para o agronegócio aos mais de 4,8 mil produtores rurais filiados das Cooperativas Frísia, Castrolanda e Capal, além dos agricultores contribuintes, como os da Coopagricola (Ponta Grossa-PR) e do grupo e BWJ (Formosa-GO).</p>
        <p>Nosso trabalho abrange uma área de 451,5 mil hectares, além de uma bacia leiteira de mais de 1,5 milhão de litros diários.</p>
        <p>A instituição também realiza projetos de pesquisa com empresas privadas, através de contratos de cooperação técnica e mantém vínculos com empresas de pesquisa pública como IAPAR, EMBRAPA e importantes universidades do Brasil.</p>
        <p>A sede está localizada na rodovia PR 151, km 288, no município de Castro-PR. É onde ficam os laboratórios. Além disso, possui quatro Campos Demonstrativos e Experimentais, que totalizam uma área aproximada em 205 hectares.</p>

        </p>
        <h2>Missão</h2>
        <p>Desenvolver soluções tecnológicas para o agronegócio, fornecendo diferencias competitivos aos produtores contribuintes e cooperativas mantenedoras.</p>
        <h2>Visão</h2>
        <p>Ser a melhor do Brasil em soluções tecnológicas sustentáveis para o agronegócio.</p>
        <h2>Valores</h2>
        <ul>
          <li>Ética e transparência</li>
          <li>Inovação</li>
          <li>Respeito ao ser humano</li>
          <li>Valorização das pessoas</li>
          <li>Respeito ao meio ambiente</li>
        </ul>
      </div>
    </div>
    <?php include("view/layout/footer.php");?>
  </div>
</body>

</html>